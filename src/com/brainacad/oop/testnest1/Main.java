package com.brainacad.oop.testnest1;

/*
 Обчислити об'єм кулі. Радіус задається аргументом командного рядка.Обчислити площу вписаного у неї кубу.
 V = 4/3 Pi*r^3
 */
public class Main {

    public static void main(String[] args) {
        if(args.length != 1){
            System.out.println("USAGE: java Main <radius>");
            System.exit(0);
        }
        String argument = args[0];
        double radius = Double.parseDouble(argument);
        //double volume = 4/3 * 3.1415 * radius*radius*radius;
        double volume = 4/3 * Math.PI * Math.pow(radius,3);
        System.out.println("Volume of sphere with radius " + radius + " is: " + volume);
    }
}

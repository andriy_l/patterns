package headfirst.p1_DuckLake_strategy;

import headfirst.p1_DuckLake_strategy.Behavior.FlyWithWings;
import headfirst.p1_DuckLake_strategy.Behavior.Quack;

/**
 * Created by andriy on 6/24/17.
 */
public class MallardDuck extends Duck {
    // quackBehavior i flyBehavior наслідуються від класу Duck
    public MallardDuck() {
        quackBehavior = new Quack(); // відповідальність за квакання лежить на Quack
        flyBehavior = new FlyWithWings(); // аналогічно відповідальним за польоти є FlyWithWings
    }

    @Override
    public void display() {
        System.out.println("I'm a real Mallard duck");
    }
}

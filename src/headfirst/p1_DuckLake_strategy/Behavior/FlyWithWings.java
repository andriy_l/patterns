package headfirst.p1_DuckLake_strategy.Behavior;

/**
 * Created by andriy on 6/22/17.
 */
public class FlyWithWings implements FlyBehavior {
    @Override
    public void fly() {
        // fly implementaionFlyWithWings
        System.out.println("I'm Flying with wings");
    }
}

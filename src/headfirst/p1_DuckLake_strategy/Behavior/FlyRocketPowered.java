package headfirst.p1_DuckLake_strategy.Behavior;

/**
 * реактивний політ качки
 */
public class FlyRocketPowered implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("I'm flying with a rocket!");
    }
}

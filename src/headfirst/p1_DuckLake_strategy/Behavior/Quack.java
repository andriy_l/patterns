package headfirst.p1_DuckLake_strategy.Behavior;

/**
 * Created by andriy on 6/22/17.
 */
public class Quack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("quacking of normal duck: Quak");
    }
}

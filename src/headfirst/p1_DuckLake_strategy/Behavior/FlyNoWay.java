package headfirst.p1_DuckLake_strategy.Behavior;

/**
 * Created by andriy on 6/22/17.
 */
public class FlyNoWay implements FlyBehavior {
    @Override
    public void fly() {
        // empty implementaion of FlyWithWings
        System.out.println("I can't fly");
    }
}

package headfirst.p1_DuckLake_strategy.Behavior;

/**
 * Created by andriy on 6/22/17.
 */
public interface FlyBehavior {
    void fly();
}

package headfirst.p1_DuckLake_strategy.Behavior;

/**
 * Created by andriy on 6/22/17.
 */
public class MuteQuack implements QuackBehavior {
    @Override
    public void quack() {
        // empty implementation
        System.out.println("<< Silence >>");
    }
}

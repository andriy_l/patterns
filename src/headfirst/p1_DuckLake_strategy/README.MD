The Strategy Pattern defines a family of algorithms,
encapsulates each one, and makes them interchangeable.
Strategy lets the algorithm vary independently from
clients that use it.

Для реалызації різних варіантів поведінки використовується патерн "Стратегія": 
- поведінка інкапсулюється в окремому наборі класів, який легко розширюється
 і змінюється, за необхідності навіть при виконанні.
 
 Стратегія визначає сімейство алгоритмів, інкапсулює і забезпечує їх взаємозамінність.
 Патерн Стратегія дає змогу модифікувати алгоритми незалежно від їх використання на стороні клієнта.
 
 
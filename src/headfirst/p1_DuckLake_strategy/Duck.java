package headfirst.p1_DuckLake_strategy;

import headfirst.p1_DuckLake_strategy.Behavior.FlyBehavior;
import headfirst.p1_DuckLake_strategy.Behavior.QuackBehavior;

/**
 * Created by andriy on 6/22/17.
 */
public abstract class Duck {

    // мають мати default access, бо не будуть бачитися наслідниками
    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior; //  кожний об'єкт міститься посилання на реалізацію інтерфейсу QuackBehavior


    // Делегування операції класам поведінки
    public void performQuack() {
        quackBehavior.quack(); // об'єкт Duck делегує поведінку об'єкту на який зсилається QuackBehavior
    }

    // perform - виконувати
    public void performFly(){
        flyBehavior.fly();
    }

    public void swim(){
        System.out.println("All ducks float, even decoys!");
        // Всі качки плавають, навіть качки-приманки!
    }

    public void display(){

    }

    // задання зміни поведінки через set-методи
    public void setFlyBehavior(FlyBehavior fb) {
        flyBehavior = fb;
    }

    public void setQuackBehavior(QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }
}


package headfirst.p1_DuckLake_strategy;

import headfirst.p1_DuckLake_strategy.Behavior.FlyRocketPowered;

/**
 * Created by andriy on 6/24/17.
 */
public class MiniDuckSimulator {
    public static void main(String[] args) {
        Duck mallard = new MallardDuck();
        mallard.performQuack();
        mallard.performFly();

        Duck model = new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }
}

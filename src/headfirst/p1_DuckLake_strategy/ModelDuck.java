package headfirst.p1_DuckLake_strategy;

import headfirst.p1_DuckLake_strategy.Behavior.FlyNoWay;
import headfirst.p1_DuckLake_strategy.Behavior.Quack;

/**
 * Качка-приманка
 */
public class ModelDuck extends Duck{
    public ModelDuck() {
        // качка-приманка не вміє літати
        flyBehavior = new FlyNoWay();
        quackBehavior = new Quack();
    }

    @Override
    public void display() {
        System.out.println("I'm a model duck");
    }
}

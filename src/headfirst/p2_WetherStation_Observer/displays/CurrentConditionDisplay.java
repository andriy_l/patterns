package headfirst.p2_WetherStation_Observer.displays;

import headfirst.p2_WetherStation_Observer.subjects.Subject;

/**
 * Created by andriy on 6/28/17.
 */
public class CurrentConditionDisplay implements Observer, DisplayElement {
    private double temp;
    private double humidity;
    private Subject weatherData;

    public CurrentConditionDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        this.weatherData.registerObserver(this);
    }

    @Override
    public void update(double temp, double humidity, double pressure) {
        this.temp = temp;
        this.humidity = humidity;
        display();
    }

    @Override
    public String toString() {
        return "Current Conditions: " +
                "temp=" + temp +
                " C degrees and humidity=" + humidity +
                "%}";
    }

    @Override
    public void display() {
        System.out.println(this.toString());
    }
}

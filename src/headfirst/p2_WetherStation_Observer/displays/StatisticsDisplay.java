package headfirst.p2_WetherStation_Observer.displays;

import headfirst.p2_WetherStation_Observer.subjects.Subject;

import java.util.ArrayList;

/**
 * Created by andriy on 6/28/17.
 */
public class StatisticsDisplay implements Observer, DisplayElement {
    private ArrayList<Double> temp;
    private ArrayList<Double> humidity;
    private ArrayList<Double> pressure;
    private Subject weatherData;

    public StatisticsDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        this.weatherData.registerObserver(this);
        temp = new ArrayList<>();
        humidity = new ArrayList<>();
        pressure = new ArrayList<>();
    }

    @Override
    public void update(double temp, double humidity, double pressure) {
        this.temp.add(temp);
        this.humidity.add(humidity);
        this.pressure.add(pressure);
        display();
    }

    @Override
    public String toString() {
        return "Statistics: " +
                "AVG/MAX/MIN=" + temp.get(temp.size()-1)+
                " C degrees and humidity=" + humidity.get(humidity.size()-1) +
                "%}";
    }

    @Override
    public void display() {
        System.out.println(this.toString());
    }
}

package headfirst.p2_WetherStation_Observer.displays;

import headfirst.p2_WetherStation_Observer.subjects.Subject;

/**
 * Created by andriy on 6/28/17.
 */
public class ForecastDisplay implements Observer, DisplayElement {
    private double temp;
    private double humidity;
    private Subject weatherData;

    public ForecastDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        this.weatherData.registerObserver(this);
    }

    @Override
    public void update(double temp, double humidity, double pressure) {
        this.temp = temp;
        this.humidity = humidity;
        display();
    }


    @Override
    public void display() {
        System.out.println("not implemented yet!");
    }
}

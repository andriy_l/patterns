package headfirst.p2_WetherStation_Observer.displays;

/**
 * Created by andriy on 6/28/17.
 */
public interface DisplayElement {
    public void display();
}

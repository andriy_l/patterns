package headfirst.p2_WetherStation_Observer.subjects;

import headfirst.p2_WetherStation_Observer.displays.Observer;

import java.util.ArrayList;

/**
 * Вміє отримувати дані від фізичного об'єкта (метеостанції), а потім
 * цей об'єкт поновлює зображення для трьох основних елементів:
 * - поточного стану (температура, вологість і тиск)
 * - статистика
 * - прогноз
 */
public class WeatherData implements Subject {
    private ArrayList<Observer> observers;
        private double temp;
        private double humidity;
        private double pressure;

    public void setMeasurements(double temp, double humidity, double pressure) {
        this.temp = temp;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public WeatherData() {
        this.observers = new ArrayList<>();
    }

    /**
     * метод викликається при надходженні нових даних з давачів
     */
    public void measurementsChanged(){
            notifyObservers();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (int i = 0; i < observers.size(); i++) {
            observers.get(i).update(temp, humidity, pressure);
        }
    }


}

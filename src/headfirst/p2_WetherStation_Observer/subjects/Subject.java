package headfirst.p2_WetherStation_Observer.subjects;

import headfirst.p2_WetherStation_Observer.displays.Observer;

/**
 * Created by andriy on 6/28/17.
 */
public interface Subject {
    // всі методи отримують в аргументі реалізацію  Observer
    public void registerObserver(Observer o);
    public void removeObserver(Observer o);

    public void notifyObservers();

}

package headfirst.p2_WetherStation_Observer;

import headfirst.p2_WetherStation_Observer.displays.CurrentConditionDisplay;
import headfirst.p2_WetherStation_Observer.displays.ForecastDisplay;
import headfirst.p2_WetherStation_Observer.displays.StatisticsDisplay;
import headfirst.p2_WetherStation_Observer.subjects.WeatherData;

/**
 * Created by andriy on 6/28/17.
 */
public class WeatherStation {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();

        CurrentConditionDisplay currentConditionDisplay = new CurrentConditionDisplay(weatherData);
        StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
        ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);

        weatherData.setMeasurements(25, 50, 700);
        weatherData.setMeasurements(28, 60, 750);
        weatherData.setMeasurements(29, 75, 740);
    }
}

Abstract Factory patterns work around a super-factory which creates other factories.
This factory is also called as factory of factories.
This type of design pattern comes under creational pattern as this pattern provides one of the best ways to create an object.

In Abstract Factory pattern an interface is responsible for creating
a factory of related objects without explicitly specifying their classes.
Each generated factory can give the objects as per the Factory pattern.

Abstract Factory Design Pattern Banefits

   - Abstract Factory design pattern provides approach to code for interface rather than implementation.
   - Abstract Factory pattern is “factory of factories” and can be easily extended to accommodate more products,
   for example we can add another sub-class Laptop and a factory LaptopFactory.
   - Abstract Factory pattern is robust and avoid conditional logic of Factory pattern.

   Abstract Factory Design Pattern Examples in JDK

       javax.xml.parsers.DocumentBuilderFactory#newInstance()
       javax.xml.transform.TransformerFactory#newInstance()
       javax.xml.xpath.XPathFactory#newInstance()